package com.epamtc.jwd.task.crew;

import java.util.Date;

public class Engineer extends CrewMember {
    public Engineer(int age, Date started) {
        super(age, started);
    }
}
