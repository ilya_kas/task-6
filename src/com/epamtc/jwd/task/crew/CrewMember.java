package com.epamtc.jwd.task.crew;

import java.util.Date;

public abstract class CrewMember extends Person{
    private final Date startOfCareer;

    public CrewMember(int age, Date started) {
        super(age);
        this.startOfCareer = started;
    }

    public int getAge() {
        return age;
    }

    public Date getStartOfCareer() {
        return startOfCareer;
    }
}
