package com.epamtc.jwd.task.plane;

import com.epamtc.jwd.task.crew.Person;
import com.epamtc.jwd.task.exception.NoSeatsLeftException;
import com.epamtc.jwd.task.exception.NullPointerException;

import java.util.Arrays;
import java.util.List;

public class Airliner extends Plane{
    private final int seatCount = 50;
    private List<Person> passengers;

    public Airliner(Person[] init) {
        this.passengers = Arrays.asList(init);
        fuelCapacity = 1000;
        liftingCapacity = 5000;
        fuelConsumption = 80;
    }

    public List<Person> getPassengers() {
        return passengers;
    }

    public void addPassenger(Person newbie) throws NullPointerException, NoSeatsLeftException {
        if (newbie==null)
            throw new NullPointerException();
        if (passengers.size()==seatCount)
            throw new NoSeatsLeftException();
        passengers.add(newbie);
    }

    public void removeCargo(Person odd) throws NullPointerException {
        if (!passengers.contains(odd))
            throw new NullPointerException();
        passengers.remove(odd);
    }
}
