package com.epamtc.jwd.task.crew;

public class Passenger extends Person{
    private int seat;

    public Passenger(int age, int seat) {
        super(age);
        this.seat = seat;
    }

    public int getSeat() {
        return seat;
    }

    public void setSeat(int seat) {
        this.seat = seat;
    }
}
