package com.epamtc.jwd.task.crew;

import java.util.Date;

public class Pilot extends CrewMember{
    public Pilot(int age, Date started) {
        super(age, started);
    }
}
