package com.epamtc.jwd.task.exception;

public class NoPayloadLeftException extends Exception{
    public NoPayloadLeftException() {
        super();
    }

    public NoPayloadLeftException(String message) {
        super(message);
    }

    public NoPayloadLeftException(String message, Throwable cause) {
        super(message, cause);
    }

    public NoPayloadLeftException(Throwable cause) {
        super(cause);
    }
}
