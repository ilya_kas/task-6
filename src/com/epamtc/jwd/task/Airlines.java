package com.epamtc.jwd.task;

import com.epamtc.jwd.task.exception.NullPointerException;
import com.epamtc.jwd.task.plane.Plane;
import com.epamtc.jwd.task.util.PlaneComparator;

import java.util.ArrayList;

public class Airlines {
    private ArrayList<Plane> planes;

    public Airlines(ArrayList<Plane> planes) throws NullPointerException {
        if (planes == null)
            throw new NullPointerException();
        this.planes = planes;
    }

    public int size() {
        return planes.size();
    }

    public void addPlane(Plane newbie)  throws NullPointerException {
        if (newbie==null)
            throw new NullPointerException();
        planes.add(newbie);
    }

    public void removePlane(Plane old) throws NullPointerException {
        if (!planes.contains(old))
            throw new NullPointerException();
        planes.remove(old);
    }

    public ArrayList<Plane> getPlanes() {
        return planes;
    }

    public double getSumLiftingCapacity(){
        double capacity = 0;
        for (Plane plane : planes)
            capacity += plane.getLiftingCapacity();
        return capacity;
    }

    public int getSumPassengerCapacity(){
        int capacity = 0;
        for (Plane plane : planes)
            capacity += plane.getFuelCapacity();
        return capacity;
    }

    public void sort(){
        planes.sort(PlaneComparator.getInstance());
    }

    /**
     * including ends
     */
    public ArrayList<Plane> filter(double min, double max) {
        ArrayList<Plane> result = new ArrayList<>();
        for (Plane plane : planes)
            if (plane.getFuelConsumption() >= min && plane.getFuelConsumption() <= max)
                result.add(plane);

        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Plane plane:planes)
            sb.append(plane.getClass()).append(", экипаж: ").append(plane.getMembers().size()).append("\n");
        return sb.toString();
    }
}
