package com.epamtc.jwd.task.exception;

public class NoSeatsLeftException extends Exception{
    public NoSeatsLeftException() {
        super();
    }

    public NoSeatsLeftException(String message) {
        super(message);
    }

    public NoSeatsLeftException(String message, Throwable cause) {
        super(message, cause);
    }

    public NoSeatsLeftException(Throwable cause) {
        super(cause);
    }
}
