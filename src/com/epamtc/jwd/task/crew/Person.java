package com.epamtc.jwd.task.crew;

public abstract class Person {
    protected final int age;

    protected Person(int age) {
        this.age = age;
    }
}
