package com.epamtc.jwd.task.plane;

import com.epamtc.jwd.task.exception.NoPayloadLeftException;
import com.epamtc.jwd.task.exception.NullPointerException;
import com.epamtc.jwd.task.util.Cargo;

import java.util.ArrayList;
import java.util.List;

public class CargoPlane extends Plane{
    private int cargoMass = 8000;
    private List<Cargo> cargo = new ArrayList<>();

    public CargoPlane() {
        fuelCapacity = 1000;
        liftingCapacity = 10000;
        fuelConsumption = 100;
    }

    public void addCargo(Cargo newbie) throws NoPayloadLeftException, NullPointerException {
        if (newbie==null)
            throw new NullPointerException();
        if (newbie.getMass()> cargoMass)
            throw new NoPayloadLeftException();
        cargo.add(newbie);
        cargoMass -= newbie.getMass();
    }

    public void removeCargo(Cargo odd) throws NullPointerException {
        if (!cargo.contains(odd))
            throw new NullPointerException();
        cargo.remove(odd);
    }

    public Cargo getCargo(int nom) {
        return cargo.get(nom);
    }

    public int getLeftCargoWeight() {
        return cargoMass;
    }
}
