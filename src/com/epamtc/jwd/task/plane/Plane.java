package com.epamtc.jwd.task.plane;

import com.epamtc.jwd.task.crew.CrewMember;
import com.epamtc.jwd.task.exception.NullPointerException;

import java.util.ArrayList;
import java.util.List;

public abstract class Plane {
    protected int fuelCapacity;           //max people count
    protected double liftingCapacity;
    protected double fuelConsumption;
    private final List<CrewMember> members = new ArrayList<>();

    public int getFuelCapacity() {
        return fuelCapacity;
    }

    public double getLiftingCapacity() {
        return liftingCapacity;
    }

    public double getFuelConsumption() {
        return fuelConsumption;
    }

    public List<CrewMember> getMembers() {
        return members;
    }

    public void addMember(CrewMember crewmate){
        if (crewmate==null)
            return;
        members.add(crewmate);
    }

    public void removeMember(CrewMember crewMember) throws NullPointerException {
        if (!members.contains(crewMember))
            throw new NullPointerException();
        members.remove(crewMember);
    }
}
