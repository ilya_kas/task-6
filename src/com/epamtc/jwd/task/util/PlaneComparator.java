package com.epamtc.jwd.task.util;

import com.epamtc.jwd.task.plane.Plane;

public class PlaneComparator implements java.util.Comparator<Plane> {

    private static final PlaneComparator self = new PlaneComparator();

    private PlaneComparator(){}

    public static PlaneComparator getInstance(){
        return self;
    }

    @Override
    public int compare(Plane o1, Plane o2) {
        return (int) (o1.getLiftingCapacity()- o2.getLiftingCapacity());
    }
}
