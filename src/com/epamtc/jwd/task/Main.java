package com.epamtc.jwd.task;

import com.epamtc.jwd.task.crew.Engineer;
import com.epamtc.jwd.task.crew.Passenger;
import com.epamtc.jwd.task.crew.Person;
import com.epamtc.jwd.task.crew.Pilot;
import com.epamtc.jwd.task.exception.NoPayloadLeftException;
import com.epamtc.jwd.task.exception.NullPointerException;
import com.epamtc.jwd.task.plane.Airliner;
import com.epamtc.jwd.task.plane.CargoPlane;
import com.epamtc.jwd.task.plane.Plane;
import com.epamtc.jwd.task.util.Cargo;

import java.util.ArrayList;
import java.util.Date;

public class Main {

    private static final double MIN_FUEL_CONSUMPTION = 10;
    private static final double MAX_FUEL_CONSUMPTION = 100;

    public static void main(String[] args) {
        ArrayList<Plane> planes = getBasicState();
        Airlines airline = null;
        try {
            airline = new Airlines(planes);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        System.out.println("Суммарная грузоподьёмность: " + airline.getSumLiftingCapacity());
        System.out.println("Вместимость человек: " + airline.getSumPassengerCapacity());

        System.out.println(airline);
        airline.sort();
        System.out.println(airline);

        ArrayList<Plane> planesInFuelPeriod = airline.filter(MIN_FUEL_CONSUMPTION, MAX_FUEL_CONSUMPTION);
        System.out.println(planesInFuelPeriod.size());
    }

    private static ArrayList<Plane> getBasicState() {
        ArrayList<Plane> planes = new ArrayList<>();

        CargoPlane newbie = new CargoPlane();
        newbie.addMember(new Pilot(37, new Date(12400)));
        newbie.addMember(new Engineer(47, new Date(11400)));
        try {
            newbie.addCargo(new Cargo(20));
        } catch (NoPayloadLeftException | NullPointerException e) {
            e.printStackTrace();
        }
        newbie.addMember(new Pilot(35, new Date(14000)));
        planes.add(newbie);

        newbie = new CargoPlane();
        newbie.addMember(new Pilot(37, new Date(12400)));
        newbie.addMember(new Engineer(27, new Date(1400)));
        newbie.addMember(new Engineer(47, new Date(11400)));
        try {
            newbie.addCargo(new Cargo(40));
        } catch (NoPayloadLeftException | NullPointerException e) {
            e.printStackTrace();
        }
        newbie.addMember(new Pilot(42, new Date(15000)));
        planes.add(newbie);

        Airliner airliner = new Airliner(new Person[]{new Passenger(30, 10)});
        airliner.addMember(new Pilot(37, new Date(12400)));
        planes.add(airliner);

        airliner = new Airliner(new Person[]{new Passenger(30, 10)});
        airliner.addMember(new Pilot(37, new Date(12800)));
        airliner.addMember(new Engineer(57, new Date(14400)));
        planes.add(airliner);

        return planes;
    }
}